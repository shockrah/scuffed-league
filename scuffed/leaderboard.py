from flask import Blueprint
from flask import jsonify
from os import environ
from scuffed.db import get_conn

board = Blueprint('leaderboard', __name__, url_prefix='/api/leaderboard')

@board.route('/state/<mode>/<region>')
def state_by_mode_region(region, mode):
    '''
    Basically we search for all the teams in a particular mode+region
    '''
    if mode not in environ['GAME_MODES']:
        return jsonify(err=f'Mode <{mode}> not found'), 400

    if region not in environ['REGIONS']:
        return jsonify(err=f'Region <{region}> not found'), 400

    conn = get_conn()
    cur = conn.cursor(prepared=True)
    query = 'SELECT id, name, wins, losses, points, captain, member2, member3 '\
        'FROM teams ' \
        'WHERE _mode = %s and region = %s ORDER BY points DESC'
    cur.execute(query, (mode, region,))

    lboard = []
    for tid, name, wins, losses, points, cap, m2, m3 in cur:
        lboard.append({
            'id': tid,
            'name': name,
            'wins': wins,
            'losses': losses,
            'points': points,
            'captain': cap,
            'member2_id': m2,
            'member3_id': m3
        })

    cur.close()
    conn.close()
    if len(lboard) == 0: return jsonify(err='Nothing found'), 400
    else: return jsonify(lboard)



@board.route('/region/<region>')
def current_region_state(region):
    '''
    Gets the _all_ leader boards that pertain to that region
    '''
    if region not in environ['REGIONS']:
        return jsonify(err=f'Region {region} unsupported'), 400

    # searching by the regions
    query = 'SELECT id, name, _mode, wins, losses, points, captain, member2, member3 '\
        'FROM teams'\
        'WHERE region = %s ORDER BY points DESC'
    conn = get_conn()
    cur = conn.cursor(prepared=True)
    cur.execute(query, (region,))

    lboard = []
    for tid, name, mode, wins, losses, points, cap , m2, m3 in cur:
        lboard.append({
            'id': tid,
            'name': name,
            'mode': mode,
            'wins': wins,
            'losses': losses,
            'points': points,
            'captain': cap,
            'member2_id': m2,
            'member3_id': m3,
        })

    cur.close()
    conn.close()

    return jsonify(lboard)

    

@board.route('/mode/<mode>')
def current_mode_global(mode):
    '''
    Gets all the leaderboards for a given mode
    Kinda like getting the global stats
    '''
    if mode not in environ.get('', False):
        return jsonify(err=f'Mode: {mode} was not found'), 400

    q = 'SELECT id, name, region, wins, losses, points, captain, member2, member3'\
        'FROM teams'\
        'WHERE _mode = %s ORDER BY points DESC'

    conn.get_conn()
    cur = conn.cursor(prepared=True)
    
    lboard = []
    for tid, name, region, wins, losses, cap, m2, m3 in cur:
        lboard.append({
            'id': tid,
            'name': name,
            'region': region,
            'wins': wins,
            'losses': losses,
            'points': points,
            'member2_id': m2,
            'member3_id': m3,

        })

    cur.close()
    conn.close()
    return jsonify(lboard)

