from flask import Blueprint
from flask import jsonify
from os import environ

from scuffed.db import get_conn
# API is open to anyone to use but will later be rate limited
# TODO: rate limiting

matches = Blueprint('matches', __name__, url_prefix='/api/matches')

FFL = -1

@matches.route('/history/<team_id>')
def team_history(team_id):
    '''
    Gets all matches that team has been a part of
    '''
    try:
        team_id = int(team_id)
    except ValueError:
        return jsonify({'errText':'Parameter must be an integer'})

    print(f'team_id: {team_id}')
    conn = get_conn()
    cur = conn.cursor(prepared=True)
    query = 'SELECT * FROM matches WHERE team1 = %s or team2i = %s'
    cur.execute()
    history = []
    for mid, t1, t2, day, t1_score, t2_score in cur:
        history.append({
            'id': mid,
            't1_id': t1,
            't2_id': t2,
            'day': day,
            't1_score': t1_score,
            't2_score': t2_score
        })
    cur.close()
    conn.close()
    return jsonify(history)

@matches.route('/mode/<mode>')
def matches_mode(mode):
    '''
    Get the matches for a particular mode
    '''
    if mode not in environ['GAME_MODES'].split():
        return f'Mode: <{mode}> not found', 400

    query = 'SELECT id, team1, team2, day, team1_score, team2_score '\
        'FROM matches WHERE mode = %s'
    conn = get_conn()
    cur = conn.cursor(prepared=True)
    cur.execute(query, (mode,))

    history = []
    for mid, t1, t2, day, t1_s, t2_s in cur:
        history.append({
            'id': mid,
            't1_id': t1,
            't2_id': t2,
            'day': day,
            't1_score': t1_s,
            't2_score': t2_s
        })


    cur.close()
    conn.close()
    return jsonify(history)

def matches_bad_request(e):
    return jsonify(err='The parameters given could not be understood')
