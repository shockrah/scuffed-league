import unittest, flask_testing
from flask_testing import TestCase
from os import environ

from scuffed import app

def debug_log(fstring):
    if environ.get('TESTING', False):
        print(fstring)

class test_matches(TestCase):
    def create_app(self):
        return app

    def test_get_matches_by_mode(self):
        # get all matches for all supported modes
        # ERESULT: good
        for mode in environ['GAME_MODES'].split():
            response = self.client.get(f'/api/matches/mode/{mode}')
            self.assert200(response, f'MODE: {mode}\nDATA: {response.data}')


    def test_matches_by_region(self):
        # ERESULT: fail
        # quick test for failure cases
        for mode in ['wowienot', 'asdf', 'asdfasdf']:
            response = self.client.get(f'/api/matches/mode/{mode}')
            self.assert400(response, f'MODE: {mode}\nDATA: {response.data}')

    def test_sqli_matches_mode(self):
        # ERESULT: fail
        # making sure sqli wont work
        for sqli in [';--SELECT * FROM users;', '10; SELECT * FROM users']:
            debug_log(f'{sqli}')
            response = self.client.get(f'/api/matches/mode/{sqli}')
            self.assert400(response, f'MODE: {sqli}\nDATA: {response.data}')

    '''
    LEADERBOARDS LEADERBOARDS LEADERBOARDS LEADERBOARDS
    LEADERBOARDS                           LEADERBOARDS
    LEADERBOARDS LEADERBOARDS LEADERBOARDS LEADERBOARDS
    '''

    def test_state_by_mode_and_region(self):
        # ERESLT: pass

        modes = environ['GAME_MODES'].split()
        regions = environ['REGIONS'].split()

        for region in regions:
            for mode in modes:
                response = self.client.get(f'/api/leaderboard/state/{mode}/{region}')
                print(f'\nMODE {mode} | REGION {region}\t DATA {response.data}')
                # dev database literally only has tdm and na data
                if region == 'na' and mode == 'tdm' :
                    self.assert200(response, f'{mode}:{region}\nDATA:{response.data}')
                else:
                    self.assert400(response, f'{mode}:{region}\nDATA:{response.data}')


if __name__ == '__main__':
    unittest.main()
