from os import environ

SQL_USER=environ['SQL_USER']
SQL_PASSWORD=environ['SQL_PASSWORD']
SQL_HOST=environ['SQL_HOST']
SQL_PORT=int(environ['SQL_PORT'])
SQL_DATABASE=environ['SQL_DATABASE']
SECRET_KEY=environ['SECRET_KEY']
SESSION_TYPE=environ['SESSION_TYPE']


GAME_MODES=environ['GAME_MODES']
REGIONS=environ['REGIONS']
