from flask import session
from flask import Blueprint
from flask import render_template


pages = Blueprint('webpages', __name__, template_folder='site-templates')

@pages.route('/')
def home():
    return render_template('index.html', logged_in=session.get('secret', False))

def page_not_found(e):
    return render_template('404.html'), 404
