from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import UserMixin
from scuffed.db import get_conn

class User(UserMixin):
    '''
    Used to load users from our db when required
    '''

    def _get_user_column(self, email):
        _ = 'SELECT username, password, FROM users WHERE email = %s'
        conn = get_conn()
        cur = conn.cursor(prepared=True)
        cur.execute(_, (email,))
        val = cur.fetchone()
        if val is None:
            return None


    def __init__(self, email, password=''):
        self.email = email
        self.password = password
        self.authenticated = False
        _user = _get_user_column(email)

        # if the email is registered did they get the pw right?
        if _user is not None and check_password(_user[1], password):
            self.authenticated = True


    def set_password(self, password):
        self.password_hash = generate_password_hash(password, method='pbkdf2:sha512:100000')

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def is_authenticated(self):
        return self.authenticated

    def is_active(self):
        # All users are always active
        return self.authenticated is True

    def is_anonymous(self):
        return False

    def get_id(self):
        pass



