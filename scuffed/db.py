import mysql.connector as sql
from os import environ

def get_conn():
    return sql.connect(
        username=environ['SQL_USER'],
        password=environ['SQL_PASSWORD'],
        host=environ['SQL_HOST'],
        database=environ['SQL_DATABASE']
    )

