import scuffed
from flask import Flask, session
from flask_session import Session
import mysql.connector as sql
from dotenv import load_dotenv
from os import environ

from scuffed.webpages import pages, page_not_found
from scuffed.matches import matches, matches_bad_request
from scuffed.leaderboard import board
from scuffed.auth import auth

def create_app():
    a = Flask(__name__)
    a.config.from_pyfile('config.py')
    Session(a)
    return a

# btw the `.env` file has to be in the project root not this source directory persei
if environ.get('DEV') is not None:
    load_dotenv(verbose=True)
else:
    load_dotenv()


app = create_app()

# Registering buildprint modules to be active
app.register_blueprint(pages)
app.register_blueprint(matches)
app.register_blueprint(board)
app.register_blueprint(auth)

app.register_error_handler(404, page_not_found)
app.register_error_handler(400, matches_bad_request)


