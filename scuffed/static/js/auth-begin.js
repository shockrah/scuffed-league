// Deal with posting login/signup data
function login() {
	let email = $('#login-email').val();
	let password = $('#login-pass').val();
	console.log(email);
	if(email.len === 0){
		$('#signup-err').text('Email field empty');
		return;
	}
	if(password.len === 0) {
		$('#signup-err').text('Password field empty');
	}

	$.post('/auth/login', 
		{
			email: $('#login-email').val(),
			password: $('#login-pass').val()
		},
		// NOTE: data here is useless on success
		function(data) {
			window.location.href="/";
		}
	)
	.fail(function() {
		$('#login-err').text('Login failed');
	});
}

function signup() {
	let email = $('#signup-email').val();
	let username = $('#signup-user').val();
	let p1 = $('#signup-pass1').val();
	let p2 = $('#signup-pass2').val();

	if(email == '') {
		$('#signup-err').text('Email field empty');
		return;
	}
	if(username == '') {
		$('#signup-err').text('Username field empty');
		return;
	}

	if(p1 != p2 || p1 == '' || p2 == '' || p1.len < 8){
		$('#signup-err').text('Passwords must match');
		return;
	}

	$.post('/auth/signup', 
		{
			email: $('#signup-email').val(),
			username: $('#signup-user').val(),
			password: $('#signup-pass1').val()
		},
		// deal with session token here and refresh (later)
		function(data) {
			window.location.href = "/";
		}
	)
	.fail(function(jqXHR, textStatus, error) {
		$('#signup-err').text(jqXHR['responseText']);
	});
}

$(document).ready(function() {
	$('#btn-login').on('click', login);
	$('#btn-signup').on('click', signup);
});
