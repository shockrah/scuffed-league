# Literally just a namespace for all things related to getting lists of teams
import mysql.connector as sql
from scuffed.utils import sql_creds

# caching the sql credentials so we dont have to check the shell environment so much
creds = sql_creds()

class TeamsServerError(Exception):
    pass

class TeamsClientError(Exception):
    pass


class Teams:
    def get(page, params):
        if 'region' not in params or params['region'] not in ['na', 'eu', 'oce']:
            raise TeamsClientError('Region must be included in the query string')

        # now we start building our query
        region = params['region']
        query = f'SELECT id, teamname, captain FROM teams WHERE region=%s'

        conn = sql.connect(**creds)
        cur = conn.cursor(prepared=True)
        cur.execute(query, (region, ))

        values = [row for row in cur]
        conn.close()
        return {'status': 'success', 'teams': values}

    def make(self, captain_id: int, name: str):
        pass

    def new_member(self, team_id: int, player_id: int):
        pass
