from werkzeug.security import generate_password_hash, check_password_hash
from flask import Blueprint, jsonify
from flask import request, redirect
from flask import session
from flask_login import logout_user
import mysql.connector as sql
import hashlib, binascii, secrets
import re, os

from scuffed import db

auth = Blueprint('auth', __name__, url_prefix='/auth')

'''
HELPER FUNCTIONS
    existant_user -> bool
    insert_user -> None
'''

def existant_user(conn, email):
    cur = conn.cursor(prepared=True)

    query = 'SELECT username, password FROM users WHERE email = %s'
    cur.execute(query, (email,))
    val = cur.fetchone()

    cur.close()
    return val

def insert_user(conn, email, username, password):
    '''
    Assumes that the parameters given are ok to use as is
    '''

    cur = conn.cursor(prepared=True)

    insert = 'INSERT INTO users (email, username, password) VALUES(%s, %s, %s)'

    password = generate_password_hash(password, method='pbkdf2:sha512:100000')
    
    cur.execute(insert, (email, username, password,))
    conn.commit()
    cur.close()


'''
ROUTES for /auth route
    signup_user -> 200 | 400
    login_user -> 200 | 400
'''

@auth.route('/signup', methods=['POST'])
def signup_user():
    email = request.form.get('email')
    username = request.form.get('username')
    password = request.form.get('password')
    print(f'fields: {email} {username} {password}')

    # check for len(0) as flask can give us exactly that
    if email is None or username is None or password is None:
        return 'Missing fields', 400
    # make sure the email fits the regex
    email_regex = '\A[a-z0-9!#$%&\'*+\/=?^_‘{|}~-]+(?:\.[a-z0-9!#$%&\'*+\/=?^_‘{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\Z'
    obj = re.compile(email_regex)
    if not obj.match(email):
        print('bad email')
        return 'Email invalid', 400

    if len(password) < 8:
        return 'Password is too short', 400

    # Does that user exist already?
    conn = db.get_conn()
    if existant_user(conn, email):
        return 'Account exists', 400

    # now we insert the new user
    insert_user(conn, email, username, password)
    conn.close()
    session['secret'] = secrets.token_urlsafe(64)
    return 'Signup success'
    


@auth.route('/login', methods=['POST'])
def login_user():
    email = request.form.get('email')
    password = request.form.get('password')

    if email is None or password is None:
        return 'Login failed', 400

    conn = db.get_conn()
    # Do we have that user?
    #   return format -> (name, pwhash)
    user = existant_user(conn, email)
    if not user:
        print('user not found')
        return 'Login failed', 400

    # assuming we have the user
    if check_password_hash(user[1], password) and 'secret' not in session:
        session['secret'] = secrets.token_urlsafe(64)
        return 'Succes'

    elif email in session:
        print(f'user already logged in {session.get(email)}')
        return 'asdf', 400
    else:
        print('something else happened idk fam')
        return 'Login failed', 400

@auth.route('/logout')
def logout_user():
    print(session.get('secret'))
    session.pop('secret', None)
    return 'logged out'

