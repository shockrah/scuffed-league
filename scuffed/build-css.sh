#!/bin/sh

# Sass inputs
sass='index navbar'

cd sass/
mkdir -p ../static/css/
for i in $sass;do
	sassc $i.scss ../static/css/$i.css
done
