from os import environ
from scuffed.db import DB
from flask import Blueprint, jsonify
from flask import request

api = Blueprint('api', __name__, url_prefix='/api')

gamemodes = [m for m in environ.get('GAME_MODES').split(' ')]

@api.route('/teams/<mode>')
def get_teams(mode: str):
    '''
    /api/teams/<mode> : modes supported right now are [wipeout duel clanarena doubles]
    Optional query string parameters
    region: one of the following [na eu oce]
    '''
    if mode not in gamemodes:
        return jsonify({
            'err': f'Unsupported gamemode passed: {mode}',
            'modes': gamemodes
        }), 400

    region = request.args.get('region')
    if region is None:
        DB.select('teams', ['id', 'region', 'teamname', 'captain'])

    else:
        DB.select(
            'teams', 
            ['id', 'region', 'teamname', 'captain'], 
            'WHERE region = %s'
        )

    # let's be real we're prolly not gonna have more than a few hundred teams at best
    result = [i for i in DB._cur]
    DB.close()

    return jsonify(result)

            
