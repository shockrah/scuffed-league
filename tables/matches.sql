create table matches(
	id INTEGER NOT NULL AUTO_INCREMENT,
	team1 INTEGER NOT NULL,
	team2 INTEGER NOT NULL,
	day DATE NOT NULL,

	team1_score INTEGER,
	team2_score INTEGER,

	mode VARCHAR(16) NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (team1) REFERENCES teams(id),
	FOREIGN KEY (team2) REFERENCES teams(id)
);
