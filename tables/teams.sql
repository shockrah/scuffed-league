create table teams(
	id INTEGER NOT NULL AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,

	region VARCHAR(16)  NOT NULL,
	_mode VARCHAR(16) NOT NULL,

	captain INTEGER NOT NULL,
	member2 INTEGER,
	member3 INTEGER,

	wins INTEGER NOT NULL,
	losses INTEGER NOT NULL,
	points INTEGER NOT NULL,

	PRIMARY KEY (id),
	FOREIGN KEY (captain) REFERENCES users(id)
);

