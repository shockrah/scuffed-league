#!/bin/sh

username() {
	cat /dev/urandom | tr -dc [:alnum:] | fold -w 8 | head -1
}

email() {
	w=$(cat /dev/urandom | tr -dc [:lower:] | fold -w 8 | head -1)
	echo $w@mail.com
}

password() {
	echo password
}

for i in $(seq 1 10);do 
	name=$(username)
	mail=$(email)
	pass=$(password)
	curl -X POST -F "username=$name" -F "password=$pass" -F "email=$mail" 'localhost:5000/auth/signup'
done
