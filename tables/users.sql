create table users(
	id INTEGER NOT NULL AUTO_INCREMENT,

	email VARCHAR(255) NOT NULL,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,

	teamid INTEGER,
	dueler BOOLEAN,
	primary key (id)
);
