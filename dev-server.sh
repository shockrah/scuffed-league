#!/bin/sh

export FLASK_APP=scuffed
export FLASK_ENV=development

flask_server() {
	. bin/activate
	# this should just be set to something generic for dev purposes but real .env's
	# should never be committed
	export DEV=yes
	pip install -e .
	flask run
}

discord_bot() {
	echo TODO
}

install_deps() {
	. bin/activate
	pip install -r requirements.txt
}

unit_test() {
	. bin/activate
	python scuffed/tests.py
}

_help() {
cat <<EOF
Options:
   h [help] show this menu
   d [discord bot] run the discord bot in dev mode
   f [flask server] run the flask server in dev mode
   S Install dependancies from requirements.txt
   t Run unit tests
EOF
}

if [ -z "$1" ];then
	_help
fi

while getopts ':hdfSt' OPTION; do
	case "$OPTION" in 
		h) _help;;
		d) discord_bot;;
		f) flask_server;;
		S) install_deps;;
		t) unit_test;;
		*) _help;;
	esac
done

