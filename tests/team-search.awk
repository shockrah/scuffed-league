BEGIN {
	FS="|"
	test_count = 0
	test_pass = 0
}

# Really we only care about what tests failed
NR > 1 {
	# if something went wrong print out that test run
	if($2 != $3) {
		print $0
	}
	# otherwise we just pretend that everything is fine
	else {
		test_pass = test_pass + 1
	}
}

END {
	print "Passed: " test_pass  "/" (NR-1)
}
