#!/bin/sh


base='localhost:5000'

run() {
	expected=$1
	shift
	status=`$@ | head -1 | awk '{print $2}'` #> /dev/null
	echo $@ \| $expected \| $status
}

post='curl -s -i -X POST'
put='curl -s -i -X PUT'
get='curl -s -i -X GET'

team_search() {
	echo 'command | RESULT | EXPECTED'
	# These cases are meant to not fail so should these ever go bad we're fucked
	for i in na eu oce;do
		run 200 $post $base'/teams/ctf?region='$i
	done
	for i in na eu oce;do
		run 200 $post $base'/teams/wipeout?region='$i
	done

	for i in na eu oce;do
		run 400 $get $base'/teams/ctf?region='$i
	done

	# These on the other hand should fail
	for i in one two three;do
		run 400 $post $base'/teams/'$i
	done

}

team_search_diff() {
	team_search | awk -f ./team-search.awk
}

_help() {
	echo 'Options: -t team_search'
}
if [ -z $1 ];then
	_help
	exit 0
fi

while getopts ':tT' OPT;do
	case "$OPT" in
		T) team_search;;
		t) team_search_diff;;
		*) _help;;
	esac
done
