# Scuffed League Website/Platform

Under Construction

A platform for competitive players and teams so that they can play with against other people who also want to play in competitive leagues.

## Why?

Too many leagues are built by and for pro players and there's nearly no grassroots movements for regular competitive players, I aim to change that.

## Currently

Website: Before the _teams_ systems are implemented I am waiting for news about what game-modes the game will officially support. This is so that the community doesn't get splintered too much by varying platforms.


Being that most of the Arena-FPS community has made the switch to discord I find it appropriate to have a way for people to arrange matches via a discord chat bot. As well as allow for some basic commands that I have known to be useful for chat moderation.

For a semi-working chat bot to setup arbitary leagues see my [League Bot](https://gitlab.com/shockrah/league-bot) project.
While nearing useful completion it still has some work to be done on it.
