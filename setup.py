from setuptools import setup

setup(
    name='scuffed',
    packages=['scuffed'],
    include_package_data=True,
    install_requires=[
        'flask'
    ],
)
