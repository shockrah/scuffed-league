CREATE TABLE IF NOT EXISTS `teams` (
    `id` int(10) PRIMARY KEY NOT NULL AUTO_INCREMENT,
	`region` varchar(10),
    `teamname` varchar(255),
    `captain` int(10),
    `player1` int(10),
    `player2` int(10),
    `player3` int(10),
    `player4` int(10),
	FOREIGN KEY (captain) REFERENCES users(id),
	FOREIGN KEY (player1) REFERENCES users(id),
	FOREIGN KEY (player2) REFERENCES users(id),
	FOREIGN KEY (player3) REFERENCES users(id),
	FOREIGN KEY (player4) REFERENCES users(id)
);
